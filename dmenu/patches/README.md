# wkd and dmenu

Which-key via **dmenu**, it's right in the name!

## Why dmenu?

Because it's highly scriptable and I don't use fzf, or rofi enough to
know if they have the desired functionality.

While patching may be required, dmenu makes the idea of `wkd` possible.

## Why patch dmenu?

Whether it be dmenu, fzf, or rofi they need to do the following things:

- Provide visual feedback about what keybinds are available.
- Strict filtering of options meaning no matching substrings.
- If one option is left return it and do not force the user to press
  return.

But there are also some nice to haves:

- Show options in a grid layout.
- Return if key entered results in no matches.

Of these behaviours dmenu only provides the first one out of the box.

## Highly recommended patches

The following patches are highly recommended:

- dmenu-exact-5.1.diff
- dmenu-instant-5.1.diff

They are both very small patches.

The exact patch is one I wrote for this project. It is very small and
provides the `-e` flag to prevent matching of substrings. Only prefixes
and exact matching is preformed when this flag is given.

The instant patch is provided by Michael Stummvoll (stummi)
`suckless@stummi.org`. This adds the `-n` flag and will select an item
instantly if it is the only option left.

With these two patches you only have to press the first letter of an
option for it to be returned to `wkd`.

Without these pressing `k`, or whatever, would match against any string
where `k` is found, and you would have to press enter to get `wkd` to
process that option.

Not exactly ideal.

## Aesthetic patches

### dmenu-xyw

This patch allows you set where on the screen dmenu should be drawn.

Read more about it
[here](https://tools.suckless.org/dmenu/patches/xyw/).

Thanks to all the contributors of this patch:

- Xarchus
- Jonathon Fernyhough (jonathon at manjaro-dot-org) (4.7 rewrite)
- Alex Cole ajzcole@airmail.cc (changes in the 5.0 version)

### dmenu-color-border

This patch adds a border to dmenu which can be specified with the `-bw`
flag. The flag takes a number to specify what size the border should be.

Useful and recommended if using the `xyw` patch.

The original patch limits the border color to the same color as the
background of a selected item.

I have added to this patch a separate border color value which you can
customize in your config.def.h

Read more about it
[here](https://tools.suckless.org/dmenu/patches/border/).

Thanks to all the contributors of this patch:

- Leon Plickat \<leonhenrik.plickat\[at\]stud.uni-goettingen.de\>
- Ben Raskin \<ben\[at\]0x1bi.net\>

### dmenu-grid

This patch provides a `-g` flag to specify the number of columns that
dmenu should display. Use this with `-l` to make a nice grid.

Read more about it
[here](https://tools.suckless.org/dmenu/patches/grid/).

Thanks to all the contributors of this patch:

- Miles Alan m@milesalan.com

### dmenu-quit-no-match

This patch adds a `-q` flag that will cause dmenu to exit if the key
entered results in no matches. Combined with the exact patch you no
longer need to hit hit the `ESC` key to exit a dmenu prompt. You still
can, but with this if you mess up a keychord with `wkd` it will exit
instead of waiting for you to enter a meaningful value.

I wrote this patch.

## Patching dmenu

For those that don't know you can apply a patch by running the following
wherever your dmenu source code lives:

``` shell
[user@localhost ~/dmenu]$ patch < patches/dmenu-exact-5.1.diff
```

If the patch fails, then inspect the `*.rej` file and manually apply the
patch.

For those a little less comfortable patching by hand you can apply these
patches in this order *hopefully* without issue:

1.  dmenu-exact-5.1.diff
2.  patch-2-instant.diff
3.  patch-3-quit-no-match.diff
4.  patch-4-xyw.diff
5.  patch-5-grid.diff
6.  patch-6-color-border.diff

This should work without issue on a clean build of dmenu but ymmv on
your own patched build. In any event they should be easy enough to
apply.
